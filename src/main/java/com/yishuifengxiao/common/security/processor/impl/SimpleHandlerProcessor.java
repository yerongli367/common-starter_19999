package com.yishuifengxiao.common.security.processor.impl;

import com.yishuifengxiao.common.security.processor.BaseHandlerProcessor;

/**
 * handler协助处理器的默认实现
 * 
 * @author yishui
 * @version 1.0.0
 * @since 1.0.0
 */
public class SimpleHandlerProcessor extends BaseHandlerProcessor {

}
